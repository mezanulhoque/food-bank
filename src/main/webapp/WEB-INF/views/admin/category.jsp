<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class="container">

    <div class="row">

        <jsp:include page="navigation.jsp"/>

        <div class="col-md-9 col-sm-9">

            <%--Add New Category Form--%>
            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading"><spring:message code="label.addNewCategory"/></div>
                <div class="panel-body">
                    <div class="col-md-4 col-sm-4">
                        <form:form action="category/add" method="post">
                            <input name="name" type="text" placeholder="Category Name"
                                   class="form-control input-md" required="required">
                            <br>
                            <button type="submit" class="btn btn-info"><spring:message code="button.add"/></button>
                        </form:form>
                    </div>
                </div>
            </div>

            <%--Available Book Category Table--%>
            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading"><spring:message code="label.availableBookCategory"/></div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><spring:message code="label.categoryId"/></th>
                            <th><spring:message code="label.categoryName"/></th>
                            <th><spring:message code="label.updateName"/></th>
                            <th><spring:message code="label.viewSubCategories"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${categories}" var="category">
                            <tr>
                                <form:form action="category/update" method="POST">
                                    <td><input name="id" value="${category.id}" readonly="true"/></td>
                                    <td><input name="name" value="${category.name}" required="required"/></td>
                                    <td>
                                        <button type="submit" class="btn btn-default">
                                            <spring:message code="button.update"/>
                                        </button>
                                    </td>
                                    <td>
                                        <a class="btn btn-info"
                                           href="${pageContext.servletContext.contextPath}/admin/category/${category.id}/subcategories">
                                            <spring:message code="label.viewSubCategories"/>
                                        </a>
                                    </td>
                                </form:form>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>