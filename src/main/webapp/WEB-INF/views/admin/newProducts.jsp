<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class="container">

    <div class="row">

        <jsp:include page="navigation.jsp"/>

        <%--New Product Request--%>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default shadow-depth-1">

                <div class="panel-heading">
                    <spring:message code="pill.productVerification"/>
                </div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><spring:message code="label.storeName"/></th>
                            <th><spring:message code="label.bookTitle"/></th>
                            <th><spring:message code="label.bookAuthor"/></th>
                            <th><spring:message code="label.bookPublisher"/></th>
                            <th><spring:message code="label.basePrice"/></th>
                            <th><spring:message code="label.discount"/></th>
                            <th><spring:message code="label.stockQuantity"/></th>
                            <th><spring:message code="label.accept"/></th>
                            <th><spring:message code="label.reject"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${newProductList}" var="product">
                            <tr>
                                <td>${product.store.name}</td>
                                <td>${product.book.title}</td>
                                <td>
                                    <c:forEach items="${product.book.authorList}" var="author">
                                        ${author.name}<br>
                                    </c:forEach>
                                </td>
                                <td>${product.book.publisher.name}</td>
                                <td>${product.basePrice}</td>
                                <td>${product.discount}</td>
                                <td>${product.stockQuantity}</td>
                                <td>
                                    <form:form action="newProducts/verify">
                                        <input name="id" value="${product.id}" type="hidden">
                                        <input name="isVerified" value="1" type="hidden">
                                        <button type="submit" class="btn btn-success">
                                            <spring:message code="button.accept"/>
                                        </button>
                                    </form:form>
                                </td>
                                <td>
                                    <form:form action="newProducts/verify">
                                        <input name="id" value="${product.id}" type="hidden">
                                        <input name="isVerified" value="2" type="hidden">
                                        <button type="submit" class="btn btn-danger">
                                            <spring:message code="button.reject"/>
                                        </button>
                                    </form:form>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
