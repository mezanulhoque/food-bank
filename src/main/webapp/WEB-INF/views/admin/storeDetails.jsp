<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class="container">

    <div class="row">

        <jsp:include page="navigation.jsp"/>

        <%--Show Particular Store--%>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default shadow-depth-1">

                <div class="panel-heading">
                    <h2><spring:message code="label.storeDetails"/></h2>
                </div>

                <div class="col-xs-12">
                    <br>

                    <div class="row">
                        <div class="col-xs-4">
                            <spring:message code="label.storeStatus"/> : ${store.status}
                        </div>
                        <div class="col-xs-4">
                            <form:form action="${store.id}/verify">
                                <c:choose>
                                    <c:when test="${store.status == 'APPROVED'}">
                                        <input name="status" value="REJECTED" type="hidden">
                                        <button type="submit" class="btn btn-danger">
                                            <spring:message code="button.reject"/>
                                        </button>
                                    </c:when>
                                    <c:otherwise>
                                        <input name="status" value="APPROVED" type="hidden">
                                        <button type="submit" class="btn btn-success">
                                            <spring:message code="button.approve"/>
                                        </button>
                                    </c:otherwise>
                                </c:choose>
                            </form:form>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-6">
                            <address>
                                <strong>Store</strong><br>
                                <spring:message code="label.name"/> : ${store.name}<br>
                            </address>
                        </div>

                        <div class="col-xs-6 text-right">
                            <address>
                                <strong><spring:message code="label.storeKeeper"/> </strong><br>
                                <spring:message code="label.name"/> : ${store.user.firstName} ${store.user.lastName}
                            </address>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><spring:message code="label.bookTitle"/></th>
                            <th><spring:message code="label.bookAuthor"/></th>
                            <th><spring:message code="label.bookPublisher"/></th>
                            <th><spring:message code="label.basePrice"/></th>
                            <th><spring:message code="label.discount"/></th>
                            <th><spring:message code="label.stockQuantity"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${storeProducts}" var="product">
                            <c:if test="${product.isVerified == '1'}">
                                <tr>
                                    <td>${product.book.title}</td>
                                    <td>
                                        <c:forEach items="${product.book.authorList}" var="author">
                                            ${author.name}<br>
                                        </c:forEach>
                                    </td>
                                    <td>${product.book.publisher.name}</td>
                                    <td>${product.basePrice}</td>
                                    <td>${product.discount}</td>
                                    <td>${product.stockQuantity}</td>
                                </tr>
                            </c:if>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>