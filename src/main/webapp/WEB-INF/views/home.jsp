<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Foodbank</title>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="<c:url value="/resources/lib/jquery-3.1.1.min.js" />"></script>
    <script src="<c:url value="/resources/lib/bootstrap/js/bootstrap.min.js" />"></script>
</head>
<body>
<c:url value="/restaurant" var="getUrl"/>
<form action="${getUrl}" method="get">
<div class="item">
    <img src="<c:url value="/resources/images/homeBackGround.jpg"></c:url>" alt="#">
    <div class="container">
        <div class="carousel-caption">
            <h1>Select A Location</h1>
            <div class="form-group">
                <select class="form-control" id="sel1" name="location">
                    <option>Mohammadpur</option>
                    <option>Banani</option>
                </select>
            </div>
            <input type="submit" class="btn btn-large btn-primary" value="Show Restaurants"/>
        </div>
    </div>
</div>
</form>
</body>
</html>