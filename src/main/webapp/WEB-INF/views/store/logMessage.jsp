<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${successMsg ne 'no' and successMsg ne null}">
    <br>
    <div class="alert success">
        <strong>Success!</strong><c:out value=" ${successMsg}"/>
    </div>
    <c:set var="successMsg" value="no" scope="session"/>
</c:if>
<c:if test="${errorMsg ne 'no' and errorMsg ne null}">
    <br>
    <div class="alert">
        <strong>Error!</strong><c:out value=" ${errorMsg}"/>
    </div>
    <c:set var="errorMsg" value="no" scope="session"/>
</c:if>
