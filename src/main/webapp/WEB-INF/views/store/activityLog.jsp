<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>

<body>
<div class="container">

    <div class="row">

        <jsp:include page="navigation.jsp"/>

        <div class="col-md-9 col-sm-9">

            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading"><spring:message code="label.activityLog"/></div>
                <div class="panel-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><spring:message code="label.createdAt"/></th>
                            <th><spring:message code="label.activityType"/></th>
                            <th><spring:message code="label.logMessage"/></th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${activityLog}" var="activityLog">
                            <tr>
                                <td><fmt:formatDate value="${activityLog.createdAt}" type="both" dateStyle="long"
                                                    timeStyle="long"/></td>
                                <td>${activityLog.activityType}</td>
                                <td>${activityLog.message}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>

                    <c:if test="${page >'0'}">
                        <a class="btn btn-default"
                           href="${pageContext.servletContext.contextPath}/mystores/activity/${page-1}"><spring:message
                                code="button.previous"/></a>
                    </c:if>

                    <c:if test="${countNextLogs >'0'}">
                        <a class="btn btn-default"
                           href="${pageContext.servletContext.contextPath}/mystores/activity/${page+1}"><spring:message
                                code="button.next"/></a>
                    </c:if>

                </div>
            </div>
        </div>
    </div>
</div>
</body>

