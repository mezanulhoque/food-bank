<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<html>
<head>
    <title>Foodbank :: Orders</title>

    <link href="<c:url value="/resources/css/user-style.css" />" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container">

    <br>
    <br>
    <br>

    <div class="row">

        <%@include file="../jspf/userNavPill.jspf" %>

        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default shadow-depth-1">
                <div class="panel-heading">
                    <h4><spring:message code="panel.myOrders"/>&nbsp;</h4>
                </div>
                <div class="panel-body">


                    <div class="table-responsive">
                        <table class="table table-striped" style="border-collapse:collapse;">
                            <thead>
                            <th>Order ID</th>
                            <th>Ordered On</th>
                            <th>Delivery On</th>
                            <th>Total Price</th>
                            <th>Payment Option</th>
                            <th>Delivery Status</th>
                            </tr>
                            </thead>
                            <tbody>

                            <c:forEach items="${orderList}" var="order">

                                <tr data-toggle="collapse" data-target="#${order.orderId}" class="accordion-toggle">
                                    <td><c:out value="# ${order.orderId}"/></td>
                                    <td><c:out value="${order.checkoutDate}"/></td>
                                    <td><c:out value="${order.deliveryDate}"/></td>
                                    <td><c:out value="${order.totalPrice}"/></td>
                                    <td><c:out value="${order.paymentInfo}"/></td>
                                    <td><span class="label label-info">
                                        <c:out value="${order.orderStatus}"/>
                                        </span>
                                    </td>
                                </tr>


                                <tr>
                                    <td colspan="11" class="hiddenRow">
                                        <div class="accordian-body collapse" id="${order.orderId}">
                                            <table class="table col-sm-12 shadow-depth-2">
                                                <thead>
                                                <tr>
                                                    <td><a href="<c:url value="/user/order-invoice/${order.orderId}"/> "
                                                           class="btn btn-success btn-sm">
                                                        <i class="glyphicon glyphicon-print"></i> show invoice</a></td>
                                                    <td></td>
                                                    <td colspan="5">
                                                        <div class="text-right pull-right"><p>
                                                            Shipping Address:<br>
                                                            <span><c:out value="${order.shippingAddress}"/></span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>Product ID</th>
                                                    <th>Cover</th>
                                                    <th>Book Name</th>
                                                    <th>Author</th>
                                                    <th>Qnty</th>
                                                    <th>Price</th>
                                                    <th>Subtotal</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <c:forEach items="${order.productList}" var="product">
                                                    <tr>
                                                        <td style="text-align: center">
                                                            <c:out value="${product.productId}"/>
                                                        </td>
                                                        <td><img class="thumbnail" height="60px"
                                                                 src="<c:url value="${product.productPhoto}"/>"/>
                                                        </td>
                                                        <td><c:out value="${product.bookTitle}"/></td>
                                                        <td><c:out value="${product.author}"/></td>
                                                        <td><c:out value="${product.quantity}"/></td>
                                                        <td>TK <c:out value="${product.price}"/></td>
                                                        <td>TK <c:out value="${product.subTotal}"/></td>
                                                    </tr>
                                                </c:forEach>

                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>

                            </c:forEach>

                            </tbody>
                        </table>


                        <ul class="pagination pull-right">

                            <c:if test="${currentPage != 0}">
                                <li><a href="<c:url value="/user/orders/${currentPage - 1}/${size}"/>">«</a></li>
                            </c:if>

                            <c:forEach begin="0" end="${noOfPages}" var="i">
                                <c:choose>
                                    <c:when test="${currentPage eq i}">
                                        <li class="active"><a>${i+1}</a></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li><a href="<c:url value="/user/orders/${i}/${size}"/>">${i+1}</a></li>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                            </li>

                            <c:if test="${currentPage lt noOfPages}">
                                <li><a href="<c:url value="/user/orders/${currentPage + 1}/${size}"/>">»</a></li>
                            </c:if>
                        </ul>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

</body>
</html>
