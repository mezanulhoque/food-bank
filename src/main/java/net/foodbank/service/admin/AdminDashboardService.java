package net.foodbank.service.admin;

import net.foodbank.dao.SaleLogDao;
import net.foodbank.web.command.BestSellarCommand;
import net.foodbank.dao.AuthTokenDao;
import net.foodbank.dao.OrderDao;
import net.foodbank.web.command.UserStatisticsCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AdminDashboardService {

    @Autowired
    OrderDao orderDao;

    @Autowired
    SaleLogDao saleLogDao;

    @Autowired
    AuthTokenDao authTokenDao;

    public List<UserStatisticsCommand> getUserStatistics() {
        return null;
    }

    public List<BestSellarCommand> getBestSellerStatistics() {
        return null;
    }
}