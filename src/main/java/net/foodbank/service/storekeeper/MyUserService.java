package net.foodbank.service.storekeeper;

import net.foodbank.domain.User;
import net.foodbank.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
@Transactional
public class MyUserService {

    @Autowired
    UserDao userDao;

    public User getUser(long id) {
        return userDao.findBy("id", id);
    }
}
