package net.foodbank.service.storekeeper;

import net.foodbank.dao.ActivityLogDao;
import net.foodbank.dao.UserDao;
import net.foodbank.domain.ActivityLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class MyActivityLogService {
    @Autowired
    ActivityLogDao activityLogDao;

    @Autowired
    UserDao userDao;

    public List<ActivityLog> getMyActivityLog(long userId, int page, int size) {
        return activityLogDao.getMyActivityLogs(userDao.find(userId), page, size);
    }
}
