package net.foodbank.service;

import net.foodbank.dao.UserProductRatingDao;
import net.foodbank.domain.UserProductRating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserProductRatingService {

    @Autowired
    private UserProductRatingDao userProductRatingDao;

    public void save(UserProductRating userProductRating) {
        userProductRatingDao.save(userProductRating);
    }
}
