package net.foodbank.service;

import net.foodbank.dao.OrderDao;
import net.foodbank.dao.UserDao;
import net.foodbank.domain.Order;
import net.foodbank.domain.User;
import net.foodbank.web.command.OrderInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Transactional
@Service(value = "userOrderService")
public class UserOrderService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private OrderDao orderDao;

    public OrderInfo getMostRecentOrderInfo(long userId){
        User user = userDao.find(userId);
        if (user != null) {

            Order order = orderDao.findMostRecentOrder(user);
            return order != null ? UserServiceHelper.getOrderInfoFromEntity(order) : null;
        }

        return null;
    }

    public OrderInfo getUserOrderInfo(long userId, long orderId) {
        User user = userDao.find(userId);
        if (user != null) {

            Order order = orderDao.findBy("user", user, "id", orderId);
            return order != null ? UserServiceHelper.getOrderInfoFromEntity(order) : null;
        }

        return null;
    }

    public List<OrderInfo> getUserOrderInfoList(long userId, int page, int size) {
        List<OrderInfo> orderInfoList = new ArrayList<>();
        User user = userDao.find(userId);

        if (user != null) {

            for (Order order : orderDao.findAllBy("user", user, page, size)) {
                orderInfoList.add(UserServiceHelper.getOrderInfoFromEntity(order));
            }
        }

        return orderInfoList;
    }

    public long getTotalOrderCount(long userId) {
        User user = userDao.find(userId);
        if (user != null) {
            return orderDao.countRows("user", user);
        }

        return 0;
    }
}
