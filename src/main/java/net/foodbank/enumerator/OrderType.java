package net.foodbank.enumerator;


public enum OrderType {
    PERSONAL, GIFT
}
