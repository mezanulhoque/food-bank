package net.foodbank.enumerator;


public enum Status {
    APPROVED, PENDING, REJECTED
}
