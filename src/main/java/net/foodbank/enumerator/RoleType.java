package net.foodbank.enumerator;


public enum RoleType {
    ADMIN, USER
}
