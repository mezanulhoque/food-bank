package net.foodbank.enumerator;


public enum Gender {
    MALE, FEMALE
}
