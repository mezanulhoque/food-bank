package net.foodbank.enumerator;


public enum OrderStatus {
    PENDING, PROCESSING, DELIVERED, DISCARDED
}