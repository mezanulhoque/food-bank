package net.foodbank.dao;

import net.foodbank.domain.Cart;
import org.springframework.stereotype.Repository;


@Repository
public class CartDao extends AbstractDao<Cart> {

}
