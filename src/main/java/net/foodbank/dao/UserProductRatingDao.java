package net.foodbank.dao;

import net.foodbank.domain.UserProductRating;
import org.springframework.stereotype.Repository;


@Repository
public class UserProductRatingDao extends AbstractDao<UserProductRating> {
}
