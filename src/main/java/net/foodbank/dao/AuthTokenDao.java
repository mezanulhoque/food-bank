package net.foodbank.dao;

import net.foodbank.domain.AuthToken;
import org.springframework.stereotype.Repository;


@Repository
public class AuthTokenDao extends AbstractDao<AuthToken> {
}
