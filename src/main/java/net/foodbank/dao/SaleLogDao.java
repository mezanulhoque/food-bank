package net.foodbank.dao;

import net.foodbank.domain.SaleLog;
import org.springframework.stereotype.Repository;


@Repository
public class SaleLogDao extends AbstractDao<SaleLog> {
}
