package net.foodbank.dao;

import net.foodbank.domain.SubCategory;
import org.springframework.stereotype.Repository;


@Repository
public class SubCategoryDao extends AbstractDao<SubCategory> {

}
