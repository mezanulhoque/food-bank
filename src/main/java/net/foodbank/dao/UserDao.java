package net.foodbank.dao;

import net.foodbank.domain.User;
import org.springframework.stereotype.Repository;


@Repository
public class UserDao extends AbstractDao<User> {
}
