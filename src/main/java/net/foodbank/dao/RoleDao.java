package net.foodbank.dao;

import net.foodbank.domain.Role;
import org.springframework.stereotype.Repository;


@Repository
public class RoleDao extends AbstractDao<Role> {
}
