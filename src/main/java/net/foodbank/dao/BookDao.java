package net.foodbank.dao;

import net.foodbank.domain.Book;
import org.springframework.stereotype.Repository;


@Repository
public class BookDao extends AbstractDao<Book> {
    public Book addBook(Book book) {
        em.persist(book);
        em.flush();
        return book;
    }
}
