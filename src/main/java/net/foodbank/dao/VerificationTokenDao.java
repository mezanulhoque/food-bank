package net.foodbank.dao;

import net.foodbank.domain.VerificationToken;
import org.springframework.stereotype.Repository;


@Repository
public class VerificationTokenDao extends AbstractDao<VerificationToken> {
}
