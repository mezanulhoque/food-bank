package net.foodbank.web.controller.admin;

import net.foodbank.service.ActivityLogService;
import net.foodbank.web.helper.AuthHelper;
import net.foodbank.domain.User;
import net.foodbank.service.UserService;
import net.foodbank.util.URL;
import net.foodbank.util.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;


@Controller
public class AdminActivityController {

    @Autowired
    ActivityLogService activityLogService;

    @Autowired
    UserService userService;

    @GetMapping(value = URL.ADMIN_ACTIVITY)
    public String getActivityLog(Locale locale,
                                 HttpServletRequest request,
                                 @PathVariable(value = "page") int page,
                                 Model model) {

        long userId = AuthHelper.getUserIdFromSession(request);
        int resultSize = 10;

        User user = userService.getUser(userId);
        model.addAttribute("activityLog", activityLogService.getActivityLog(user, page, resultSize));
        model.addAttribute("countNextLogs", activityLogService.countNextLogs(user, page + 1, resultSize));
        model.addAttribute("page", page);

        return View.ADMIN_ACTIVITY;
    }
}