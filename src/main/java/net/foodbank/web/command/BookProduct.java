package net.foodbank.web.command;

import net.foodbank.domain.Author;
import net.foodbank.domain.Book;
import net.foodbank.domain.Product;


public class BookProduct {
    private Book book;
    private Product product;
    private Author author;

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
