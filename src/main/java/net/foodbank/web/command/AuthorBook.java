package net.foodbank.web.command;

import net.foodbank.domain.Book;
import net.foodbank.domain.Author;

import java.util.List;


public class AuthorBook {
    private List<Author> authorList;
    private Book book;

    public List<Author> getAuthorList() {
        return authorList;
    }

    public void setAuthorList(List<Author> authorList) {
        this.authorList = authorList;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
